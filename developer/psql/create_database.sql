DO $$
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_user WHERE usename = 'image_gallery') THEN
        CREATE USER image_gallery login password 'temppassword';
    END IF;
END
$$;

GRANT image_gallery TO postgres;

DROP DATABASE IF EXISTS image_gallery;

CREATE DATABASE image_gallery owner image_gallery;