CREATE TABLE IF NOT EXISTS users (
    username VARCHAR(100) PRIMARY KEY NOT NULL,
    password VARCHAR(100) NOT NULL,
    full_name VARCHAR(200) NOT NULL,
    enabled BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS authorities (
    username VARCHAR(100) PRIMARY KEY NOT NULL,
    authority VARCHAR(100) NOT NULL,
    CONSTRAINT foreign_key_users_1 FOREIGN KEY (username) REFERENCES users (username)
);

CREATE TABLE IF NOT EXISTS images (
    key VARCHAR(200) PRIMARY KEY NOT NULL,
    username VARCHAR(100) NOT NULL,
    url VARCHAR(300) NOT NULL,
    CONSTRAINT foreign_key_users_1 FOREIGN KEY (username) REFERENCES users (username)
);

INSERT INTO users (username, password, full_name)
VALUES ('gas0026', 'password', 'Grover Sundstrom'),
       ('dlc0027', 'password', 'Dodam Chun')
ON CONFLICT DO NOTHING;

INSERT INTO authorities (username, authority)
VALUES ('gas0026', 'ROLE_ADMIN'),
       ('dlc0027', 'ROLE_USER')
ON CONFLICT DO NOTHING;